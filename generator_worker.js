//Include some stuff required for generating, probably a better way to do this
importScripts('const.js', 'util.js', 'chunkdata.js', 'libs/three.min.js', 'libs/perlin-noise-simplex.js');
var constant = new Constant();
this.constant = constant;
var perlin = new SimplexNoise();
this.perlin = perlin;

//Queue for storing chunks to be loaded
var queuedChunks = [];

function isAlreadyQueued(x, y) {
    for (var i in queuedChunks) {
        if (queuedChunks[i][0] === x && queuedChunks[i][1] === y) {
            return true;
        }
    }
    return false;
}

self.addEventListener('message', function (e) {
    if (e.data instanceof Array && e.data.length == 2) {
        //Queue the chunk for generation
        if (!isAlreadyQueued(e.data[0], e.data[1])) {
            //console.log("Queueing " + e.data);
            queuedChunks.push([e.data[0], e.data[1]]);
        }
    } else if (e.data.length > 0) {
        console.log("Received updated parameters.");
        constant.AMPLIFICATION_MULTIPLIER = e.data[0];
        constant.BIOME_SIZE = e.data[1];
        constant.TEMPERATURE_CUTOFF = e.data[2];
        constant.HUMIDITY_CUTOFF = e.data[3];
        constant.SHORE_CUTOFF = e.data[4];
        constant.DESERT_CUTOFF = e.data[5];
        //Changes seed
        if (e.data[6] === 1) {
            console.log("change seed!");
            perlin = new SimplexNoise();
        }
    } else {
        console.log("Error: invalid data sent to generation worker.");
    }
}, false);


function generateChunks() {
    while (queuedChunks.length > 0) {
        var chunkPos = queuedChunks.shift();
        var chunkData = new ChunkData(chunkPos);
        //Send the generated chunkData back
        console.log("begin post message");
        var t0 = performance.now();
        self.postMessage(chunkData);
        var t1 = performance.now();
        console.log("end post message, " + (t1 - t0).toFixed(4) + " ms.");
    }

    //Call generation20 times a second
    setTimeout("generateChunks()", 50);
}

generateChunks();

//function timedCount() {
//    i = i + 1;
//    postMessage(i);
//    setTimeout("timedCount()",500);
//}
//
//timedCount();