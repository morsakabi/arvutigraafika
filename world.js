var World = function () {

    perlin = new SimplexNoise();

    //A dictionary for chunks. Key is the chunk x,y position, value is the chunk itself.
    var chunks = {};
    this.chunks = chunks;
    var sentPositions = [];
    this.sentPositions = sentPositions;

    //Load some chunks for debugging
    var radius = 2;
    for (var x = -radius; x <= radius; x++) {
        for (var y = -radius; y <= radius; y++) {
            this.loadChunk(x, y);
        }
    }
    return this;
};

World.prototype = {
    loadChunk: function (x, y) {
        //Send this chunk for generation to the generator worker.	
        if (this.sentPositions.indexOf(x + "_" + y) === -1) {
            this.sentPositions.push(x + "_" + y);
            generationWorker.postMessage([x, y]);
        }

    },
    onChunkDataLoaded: function (chunkData) {

        var t0 = performance.now();
        var chunk = new Chunk(chunkData.pos, chunkData.data, chunkData.faces, chunkData.vertices);
        this.chunks[chunkData.pos] = chunk;
        chunk.buildMesh();
        chunk.mesh.name = "chunk " + chunkData.pos[0] + ", " + chunkData.pos[1];
        main.scene.add(chunk.mesh);
        var t1 = performance.now();
        //console.log('OnChunkDataLoaded took', (t1 - t0).toFixed(4), 'milliseconds');
    },
    unloadChunk: function (x, y) {
        if (this.chunks[[x, y]] !== null) {
            this.chunks[[x, y]].mesh.geometry.dispose();
            this.chunks[[x, y]].mesh.material.dispose();//don't know if you need these, even
            main.scene.remove(this.chunks[[x, y]].mesh);
            delete this.chunks[[x, y]];

            var index = this.sentPositions.indexOf(x + "_" + y);
            this.sentPositions.splice(index, 1);

            //console.log("unloaded: " + x + " " + y);
            return true;
        } else {
            return false;
        }
    },
    unloadAllChunks: function () {
        for (var i in this.chunks) {
            this.unloadChunk(this.chunks[i].pos[0], this.chunks[i].pos[1]);
        }
    },
    //Returns a boolean representing if chunk with given coordiantes is loaded or not.
    isChunkLoaded: function (x, y) {
        return this.chunks[[x, y]] !== undefined;
    },
    isInRadius: function (origin, radius, target) {
        return  target[0] <= origin[0] + radius && target[0] >= origin[0] - radius &&
                target[1] <= origin[1] + radius && target[1] >= origin[1] - radius;
    },
    updateChunks: function (pos, radius) {

        //Remove chunks that are no longer in the given radius
        for (var i in this.chunks) {
            var chunk = this.chunks[i];
            if (!this.isInRadius([pos.x, pos.y], radius + 2, chunk.pos)) {
                this.unloadChunk(chunk.pos[0], chunk.pos[1]);
            }
        }

        //Add chunks that should be now loaded
        for (var x = -radius; x <= radius; x++) {
            for (var y = -radius; y <= radius; y++) {

                if (!this.isChunkLoaded(pos.x + x, pos.y + y)) {
                    this.loadChunk(pos.x + x, pos.y + y);
                }
            }
        }
    }
};