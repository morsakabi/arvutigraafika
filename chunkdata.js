var ChunkData = function (pos) {
    this.pos = pos;

    //For debugging
    this.debugCounter = 0;

    var data = new Array(constant.chunkvolume);
    this.data = data;
    var t0 = performance.now();
    this.generate();
    var t1 = performance.now();
    //console.log('ChunkData generate took', (t1 - t0).toFixed(4), 'milliseconds');
    //generate faces and vertices
    t0 = performance.now(); 
    this.build();
     t1 = performance.now();
    //console.log('ChunkData build took', (t1 - t0).toFixed(4), 'milliseconds');
    return this;
};


ChunkData.prototype = {
    generate: function () {
        //Making this bigger makes terrain smoother
        var scale = 40;
        var scale2D = 100;
        
        for (var x = 0; x < constant.chunkwidth; x++) {
            for (var z = 0; z < constant.chunkwidth; z++) {
                var globalBlock = localBlockToGlobalBlock(this.pos[0], this.pos[1], x, 0, z);

                //Get biome relevant stuff at this x,z top-down coordinate
                var biome = this.getBiome(globalBlock.x, globalBlock.z);

                //Step one: Calculate a height value based on x and z.
                var heightVal = perlin.noise(globalBlock.x / scale2D, globalBlock.z / scale2D) * biome.amplification;
                //Clamp maxground between 3 and chunkheight so there's always some solid ground
                var maxGround = Math.max(Math.min(Math.floor(heightVal * constant.chunkheight), constant.chunkheight), 3);

                for (var y = 0; y < constant.chunkheight; y++) {
                    var i = xyzToI(x, y, z);

                    //var globalBlock = localBlockToGlobalBlock(this.pos[0], this.pos[1], x, y, z);
                    //we can reuse the globalBlock from x,z calculation, just have to change y
                    globalBlock.y = y;

                    if (y < maxGround) {
                        //Current y is lower than the maxGround value set by the 2D noise
                        if (maxGround - y < 10) {
                            //Set the 10 layers closest to the ground with the block from biome data
                            this.data[i] = biome.block;

                        } else {
                            //Make deeper underground rock
                            this.data[i] = constant.BLOCK_ROCK;
                        }
                    } else {
                        //Current y is higher than the maxGround value, so lets use 3D noise for the rest
                        // var noiseVal = perlin.noise3D(globalBlock.x / scale, globalBlock.y / scale, globalBlock.z / scale) * 2;

                        //Reduce noise as we get higher so there's less weird floating stuff
                        // noiseVal -= (y / constant.chunkheight) * 1.5;

                        // if (noiseVal > 0.5) {
                        //   this.data[i] = constant.BLOCK_ROCK;
                        //} else {
                        this.data[i] = constant.BLOCK_EMPTY;
                        //  }
                    }
                }
            }
        }

    },
    getBiome: function (x, z) {

        //Calculate some base values for generating the biome
        var amplification = perlin.noise3D((x + 12000) / constant.BIOME_SIZE, 0, (z + 13000) / constant.BIOME_SIZE) * constant.AMPLIFICATION_MULTIPLIER;

        var humidity = perlin.noise3D((x + 18000) / constant.BIOME_SIZE, 0, (z + 18000) / constant.BIOME_SIZE);

        var temperature = perlin.noise3D((x + 42000) / constant.BIOME_SIZE, 0, (z + 43000) / constant.BIOME_SIZE);

        var block = constant.BLOCK_GRASS;

        if(humidity < constant.HUMIDITY_CUTOFF && temperature < constant.TEMPERATURE_CUTOFF) {
            //GRASS: Low humidity and low temperature          
            if(humidity > constant.HUMIDITY_CUTOFF - 0.02 && temperature > constant.TEMPERATURE_CUTOFF - 0.02) {
                block = constant.BLOCK_SHORE;
            } else {
                block = constant.BLOCK_GRASS;
            }
            
            //If the value is near water, reduce amplification to prevent sharp river banks
            var delta = (constant.TEMPERATURE_CUTOFF - temperature + constant.HUMIDITY_CUTOFF - humidity) / 1;
            if(delta <= constant.SHORE_CUTOFF) {
                amplification *= this.lerp(0, 1, delta * (1 / constant.SHORE_CUTOFF));
            }
        } else if(humidity >= constant.HUMIDITY_CUTOFF && temperature < constant.TEMPERATURE_CUTOFF) {
            //SNOW: High humidity and low temperature
            block = constant.BLOCK_SNOW;
            
            //If the value is near water, reduce amplification to prevent sharp river banks
            var delta = constant.TEMPERATURE_CUTOFF - temperature;
            if(delta <= constant.SHORE_CUTOFF) {
                amplification *= this.lerp(0, 1, delta * (1 / constant.SHORE_CUTOFF));
            }
        } else if(humidity < constant.HUMIDITY_CUTOFF && temperature >= constant.TEMPERATURE_CUTOFF) {
            //DESERT: Low humidity and high temperature
            if(humidity > constant.HUMIDITY_CUTOFF - 0.02) {
                block = constant.BLOCK_SHORE;
            } else {
                block = constant.BLOCK_SAND;
            }
            
            
            //If the value is not close to the biome border, reduce amplification to make deserts more flat
            var delta = (constant.HUMIDITY_CUTOFF - humidity + temperature - constant.TEMPERATURE_CUTOFF) / 1;
            if(delta > constant.DESERT_CUTOFF) {
                amplification *= this.lerp(1, 0.3, (delta - constant.DESERT_CUTOFF) * (1 / constant.DESERT_CUTOFF));
 
                //Use this for debugging the areas covered by this
                //block = constant.BLOCK_ERROR;
            }
            
            
            //If the value is near water, reduce amplification to prevent sharp river banks
            var delta = constant.HUMIDITY_CUTOFF - humidity;
            if(delta <= constant.SHORE_CUTOFF) {
                amplification *= this.lerp(0, 1, delta * (1 / constant.SHORE_CUTOFF));
            }
            
        } else if(humidity >= constant.HUMIDITY_CUTOFF && temperature >= constant.TEMPERATURE_CUTOFF) {
            //OCEAN: High humidity and high temperature
            block = constant.BLOCK_WATER;
            //Make it flat
            amplification = 0;
            
        }

        return {
            "amplification": amplification, 
            "humidity": humidity, 
            "temperature": temperature,
            "block": block
        };
    },
    lerp: function(val1, val2, weight) {
        weight = Math.min(1, Math.max(weight, 0));
        return val1 + weight * (val2 - val1);
    },
    build: function () {
        var faces = [];
        var vertices = [];
        //offset of vertices counter
        var offset = 0;

        for (var i = 0; i < constant.chunkvolume; i++) {
            if (this.data[i] !== 0) {
                var localBlock = iToXYZ(i);
                var globalBlock = localBlockToGlobalBlock(this.pos[0], this.pos[1], localBlock.x, localBlock.y, localBlock.z);
                var globalPos = globalBlockToGlobalPosition(globalBlock.x, globalBlock.y, globalBlock.z);

                var color = getBlockColor(this.data[i]);

                for (var j = 0; j < constant.sides.length; j++) {
                    if (j === 2 && localBlock.y === 0) {
                        //Prevent wasteful rendering of bottom of the chunks
                        continue;
                    }
                    if (this.isSideVisible(constant.sides[j], localBlock)) {
                        //push 4 vertices

                        var relevantVertices = sideToVerticesArrayIndexes(constant.sides[j]);

                        for (var k = 0; k < relevantVertices.length; k++) {
                            //Get the vector with elements being either 0 or constant.blockwidth
                            var vec = constant.vertices[relevantVertices[k]];
                            vertices.push([vec.x + globalPos.x, vec.y + globalPos.y, vec.z + globalPos.z]);
                        }

                        var face1 = [offset + 0, offset + 1, offset + 2];
                        var face2 = [offset + 1, offset + 3, offset + 2];

                        face1.color = color;
                        face2.color = color;
                           
                        faces.push(face1);
                        faces.push(face2);
                        offset += 4;
                    }
                }
            }
        }

        this.faces = faces;
        this.vertices = vertices;
    },
    isSideVisible: function (side, localBlock) {
        //console.log("==========================");
        //Make a copy of the side vector so it doesnt change
        var copy = new THREE.Vector3().copy(side);

        //Add the localblock position to the copy
        copy.add(localBlock);

        //Now we have the local block position of the block that is next to the side
        //that we are currently inspecting
        if (isOnLocalChunk(copy.x, copy.y, copy.z)) {
            //console.log("is on local chunk");
            var index = pointToI(copy);

            return this.data[index] === 0;
        } else {
            //console.log("is not on local chunk");
            //TODO: The block is on another chunk, got to check that instead
            return true;
        }

    },
    isValidSide: function (side) {
        if (side.x === 0 && side.y === 0 && (side.z === -1 || side.z === 1) ||
                side.x === 0 && (side.y === -1 || side.y === 1) && side.z === 0 ||
                (side.x === -1 || side.x === 1) && side.y === 0 && side.z === 0) {
            return true;
        } else {
            return false;
        }
    }
};