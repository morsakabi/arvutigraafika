//All these functions are ported from our Robonation project.
//See https://bitbucket.org/morsakabi/robonation/src/e2f9f1520ebcde5686827089467f0348cc6ac084/Assets/Engine/Util/CoordUtil.cs?at=master&fileviewer=file-view-default
//for more extensive explanations.

//Translates a 3D x,y,z local chunk coordinates to a 1D array index.
function pointToI(point) {
    return xyzToI(point.x, point.y, point.z);
}

//Translates a 3D x,y,z local chunk coordinates to a 1D array index.
function xyzToI(x, y, z) {
    return y * constant.chunkarea + z * constant.chunkwidth + x;
}

//Translates a 1D array index to 3D x,y,z local chunk coordinates.
function iToXYZ(i) {
//Divides i by CHUNKAREA, see http://stackoverflow.com/questions/18560844/does-java-optimize-division-by-powers-of-two-to-bitshifting
    var y = i >> (constant.chunkwidthbit * 2);
    var x = modulo(i, constant.chunkwidth);
    //Divides first half by CHUNKWIDTH
    var z = (i - y * constant.chunkarea) >> (constant.chunkwidthbit);
    return new THREE.Vector3(x, y, z);
}

/** Takes x modulo y if y is a power of two. Warning: No check for y is made.
 * Read more: http://dhruba.name/2011/07/12/performance-pattern-modulo-and-powers-of-two/
 * @return x % y, if y is power of two.
 */
function modulo(x, y) {
    return x & (y - 1);
}

//Translates a local block to a global block.
function localBlockToGlobalBlock(chunkX, chunkZ, x, y, z) {
    var _x = chunkX * (constant.chunkwidth - 1) + x;
    var _z = chunkZ * (constant.chunkwidth - 1) + z;
    return new THREE.Vector3(_x, y, _z);
}

/** Translates the given Global block coordinates to bottom corner global position. */
function globalBlockToGlobalPosition(x, y, z) {
    return new THREE.Vector3(x * constant.blockwidth, y * constant.blockwidth, z * constant.blockwidth);
}

function GlobalPositionToChunk(x, y, z) {
    var globalBlock = GlobalPositionToGlobalBlock(x, y, z);
    return GlobalBlockToChunk(globalBlock.x, globalBlock.y, globalBlock.z);
}
		
function GlobalPositionToGlobalBlock(x, y, z) {
    var _x = x >= 0 ? Math.floor(x / constant.blockwidth) : Math.floor(x / constant.blockwidth);
    var _y = Math.floor(y / constant.blockwidth);
    var _z = z >= 0 ? Math.floor(z / constant.blockwidth) : Math.floor(z / constant.blockwidth);
    return new THREE.Vector3(_x, _y, _z);
}
		
function GlobalBlockToChunk(x, y, z) {
    var _x = x >= 0 ? Math.floor(x / constant.chunkwidth) : Math.floor((x - constant.chunkwidth + 1) / constant.chunkwidth);
    var _z = z >= 0 ? Math.floor(z / constant.chunkwidth) : Math.floor((z - constant.chunkwidth + 1) / constant.chunkwidth);
	
    return new THREE.Vector2(_x, _z);
}

function isOnLocalChunk(x, y, z) {
    return x >= 0 && y >= 0 && z >= 0 && x < constant.chunkwidth && y < constant.chunkwidth && z < constant.chunkwidth;
}


function sideToVerticesArrayIndexes(side) {
    if(side === constant.sides[0]) {       //0, 0, -1
        return [0, 2, 1, 3];
    } else if(side === constant.sides[1]) {//0, 0, 1
        return [4, 5, 6, 7];
    } else if(side === constant.sides[2]) {//0, -1, 0
        return [0, 1, 4, 5];
    } else if(side === constant.sides[3]) {//0, 1, 0
        return [2, 6, 3, 7];
    } else if(side === constant.sides[4]) {//-1, 0, 0
        return [0, 4, 2, 6];
    } else if(side === constant.sides[5]) {//1, 0, 0
        return [1, 3, 5, 7];
    }
    return [];
}


function getBlockColor(block) {
    if(block === constant.BLOCK_EMPTY) {
        return 0xffffff;
    } else if(block === constant.BLOCK_ROCK) {
        return 0x888888;
    } else if(block === constant.BLOCK_GRASS) {
        return 0x718d3b;
    } else if(block === constant.BLOCK_SAND) {
        return 0xcab855;
    } else if(block === constant.BLOCK_SNOW) {
        return 0xfffffa;
    } else if(block === constant.BLOCK_SHORE) {
        return 0xb4a138;
    } else if(block === constant.BLOCK_WATER) {
        return 0x002299;
    } else if(block === constant.BLOCK_ERROR) {
        return 0xff0000;
    } else if(block === constant.BLOCK_GRASS2) {
        return 0x89b438;
    }
    return 0x000000;
}