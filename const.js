var Constant = function() {

    //Should be power of 2
    this.chunkwidth = 32;
  
    //Should be power of 2
    this.chunkheight = 64;
    
    this.blockwidth = 10;

    this.chunkwidthbit = (this.chunkwidth - 1).toString(2).length;

    this.chunkarea = this.chunkwidth * this.chunkwidth;

    this.chunkvolume = this.chunkarea * this.chunkheight;
    
    //BIOME GENERATION
    this.AMPLIFICATION_MULTIPLIER = 1.0;
    this.BIOME_SIZE = 100;
    this.TEMPERATURE_CUTOFF = 0.5;
    this.HUMIDITY_CUTOFF = 0.5;
    this.SHORE_CUTOFF = 0.075;
    this.DESERT_CUTOFF = 0.15;

    //BLOCKS:
    this.BLOCK_EMPTY = 0;
    this.BLOCK_ROCK = 1;
    this.BLOCK_GRASS = 2;
    this.BLOCK_SAND = 3;
    this.BLOCK_SNOW = 4;
    this.BLOCK_SHORE = 5;
    this.BLOCK_WATER = 6;
    this.BLOCK_ERROR = 7;
    this.BLOCK_GRASS2 = 8;
    
    this.sides = [new THREE.Vector3(0, 0, -1), new THREE.Vector3(0, 0, 1),
        new THREE.Vector3(0, -1, 0), new THREE.Vector3(0, 1, 0),
        new THREE.Vector3(-1, 0, 0), new THREE.Vector3(1, 0, 0)];

//    this.vertices = [
//        -1.0, -1.0, -1.0,
//        1.0, -1.0, -1.0,
//        -1.0, 1.0, -1.0,
//        1.0, 1.0, -1.0,
//        -1.0, -1.0, 1.0,
//        1.0, -1.0, 1.0,
//        -1.0, 1.0, 1.0,
//        1.0, 1.0, 1.0
//    ];
    
    this.vertices = [
        new THREE.Vector3(0.0, 0.0, 0.0),
        new THREE.Vector3(this.blockwidth, 0.0, 0.0),
        new THREE.Vector3(0.0, this.blockwidth, 0.0),
        new THREE.Vector3(this.blockwidth, this.blockwidth, 0.0),
        new THREE.Vector3(0.0, 0.0, this.blockwidth),
        new THREE.Vector3(this.blockwidth, 0.0, this.blockwidth),
        new THREE.Vector3(0.0, this.blockwidth, this.blockwidth),
        new THREE.Vector3(this.blockwidth, this.blockwidth, this.blockwidth)
    ];

    this.faces = [
        0, 2, 1, //front
        1, 2, 3,
        4, 5, 6, //back
        5, 7, 6,
        0, 4, 2, //left
        2, 4, 6,
        1, 3, 5, //right
        3, 7, 5,
        0, 1, 4, //bottom
        1, 5, 4,
        2, 6, 3, //top
        3, 6, 7
    ];

};