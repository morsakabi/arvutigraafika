function onLoad() {
    var main = new Main();
    main.initialize();

    this.main = main;

    //Load constants
    var constant = new Constant();
    this.constant = constant;
    
    initializeUI();

    var generationWorker = startGenerationWorker();
    this.generationWorker = generationWorker;

    //Create a world
    var world = new World();
    this.world = world;

    initializeViewport(650, 450);

    draw();
}

function startGenerationWorker() {
    if (typeof (Worker) !== "undefined") {
        var worker = new Worker("generator_worker.js");

        worker.addEventListener('message', function (e) {
            //console.log('Worker sent: ', e.data);
            world.onChunkDataLoaded(e.data);
        }, false);

        return worker;
    } else {
        alert("Sorry, your browser does not support web workers. Move out of stone age and upgrade.");
        return null;
    }
}

function initializeUI() {
    var Text = function() {
        this.amplification = 1.0;
        this.biomeSize = 100;      
        this.temperatureCutoff = 0.5;
        this.humidityCutoff = 0.5;
        this.shoreCutoff = 0.075;
        this.desertCutoff = 0.15;
		this.changeSeed = false;
        this.GENERATE = function() {
            world.unloadAllChunks();
            generationWorker.postMessage([
                text.amplification,
                text.biomeSize,
                text.temperatureCutoff,
                text.humidityCutoff,
                text.shoreCutoff,
                text.desertCutoff,
                text.changeSeed ? 1 : 0
            ]);
        };      
    };
    
    var gui = new dat.GUI();  
    var f1 = gui.addFolder('Parameters');
    var text = new Text();
    f1.add(text, 'amplification', 0.1, 2).step(0.05).title('Bigger: biomes are larger');
    f1.add(text, 'biomeSize', 50, 750).step(50).title('Yes');
    f1.add(text, 'temperatureCutoff', 0.0, 1.0).step(0.05).title('low: more desert and water, high: more grass and snow');
    f1.add(text, 'humidityCutoff', 0.0, 1.0).step(0.05)
            .title('low: more snow and water, high: more desert and grass');
    f1.add(text, 'shoreCutoff', 0.05, 0.2).step(0.025).title('Smoothing factor near water shores');
    f1.add(text, 'desertCutoff', 0.05, 0.2).step(0.05).title('Smoothing factor for deserts being more even');
    f1.add(text, 'changeSeed').title('Uses different seed on new chunks');
    f1.add(text, 'GENERATE'); 
    f1.open();  
}

function initializeViewport(width, height) {
    var canvasContainer = document.getElementById('canvasContainer');
    var width = window.innerWidth - 20;
    var height = window.innerHeight - 20;
    //var width = width;
    //var height = height;

    renderer = new THREE.WebGLRenderer();
    renderer.setSize(width, height);
    renderer.setClearColor(0xeeeeee, 1);
    canvasContainer.appendChild(renderer.domElement);

    //stats component
    stats = new Stats();
    stats.domElement.style.position = 'absolute';
    stats.domElement.style.bottom = '0px';
    stats.domElement.style.zIndex = 100;
    canvasContainer.appendChild(stats.domElement);


    var lookAtVector = new THREE.Vector3(0, 0, 0);
    var camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 10000);

    camera.position.y = 800;
    camera.position.x = -100;
    camera.position.z = 0;
    camera.lookAt(lookAtVector);
    camera.updateProjectionMatrix();
    main.camera = camera;
    main.lookAtVector = lookAtVector;

    var controls = new THREE.OrbitControls(camera, this.renderer.domElement);
    main.controls = controls;
}

function draw() {
    requestAnimationFrame(draw);

    main.controls.update();
//    if (main.camera !== null) {
//        main.lookAtVector.x += 0.1;
//        main.camera.position.x += 0.1;
//        main.camera.lookAt(main.lookAtVector);
//        
//    }

    main.keyboardUpdate(main.camera);

    var currentChunkPos = GlobalPositionToChunk(main.lookAtVector.x, main.lookAtVector.y, main.lookAtVector.z);
    this.world.updateChunks(currentChunkPos, 2);


    renderer.render(main.scene, main.camera);
}


var Main = function () {
    //Public variables
    this.scene;
    this.keyboard;


    this.initialize = function () {
        var keyboard = new THREEx.KeyboardState();
        this.keyboard = keyboard;

        var scene = new THREE.Scene();
        this.scene = scene;

        var ambientLight = new THREE.AmbientLight(0x333333);
        scene.add(ambientLight);

        var directionalLight = new THREE.DirectionalLight(0xffffff, 0.8);
        directionalLight.position.set(0.6, 0.9, 0);
        scene.add(directionalLight);


    }

    this.keyboardUpdate = function (camera) {

        if (this.keyboard.pressed("w")) {
            this.lookAtVector.x += camera.getWorldDirection().x * 5.0;
            this.lookAtVector.z += camera.getWorldDirection().z * 5.0;
            camera.position.x += camera.getWorldDirection().x * 5.0;
             camera.position.z += camera.getWorldDirection().z * 5.0;
        } else if (this.keyboard.pressed("s")) {
            this.lookAtVector.x -= camera.getWorldDirection().x * 5.0;
            this.lookAtVector.z -= camera.getWorldDirection().z * 5.0;
            camera.position.x -= camera.getWorldDirection().x * 5.0;
            camera.position.z -= camera.getWorldDirection().z * 5.0;
        } else if (this.keyboard.pressed("a")) {
            this.lookAtVector.x += camera.getWorldDirection().z * 5.0;
            this.lookAtVector.z -= camera.getWorldDirection().x * 5.0;
            camera.position.x += camera.getWorldDirection().z * 5.0;
            camera.position.z -= camera.getWorldDirection().x * 5.0;
        } else if (this.keyboard.pressed("d")) {
            this.lookAtVector.x -= camera.getWorldDirection().z * 5.0;
            this.lookAtVector.z += camera.getWorldDirection().x * 5.0;
            camera.position.x -= camera.getWorldDirection().z * 5.0;
             camera.position.z += camera.getWorldDirection().x * 5.0;
        }
        
        this.controls.center = this.lookAtVector;
        camera.lookAt(this.lookAtVector);

    }

    return this;
}




