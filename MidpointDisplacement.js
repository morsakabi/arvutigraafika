
var midpointWidth = 128;
this.max = midpointWidth;

var midpointData = new Array(midpointWidth, midpointWidth, midpointWidth );

randomizeCorners();

//Dont forget to change iteration to log2 (midpointWidth) when changing midpointWidth
for(var iteration = 7; iteration >= 1; iteration--) {
	var pow2 = Math.pow(2, iteration);
	var length = midpointWidth / pow2;
	//console.log("Iteration " + iteration + " pow2 " + pow2 + " length " + length);
	for(var x = 0; x < midpointWidth; x += pow2) {
		for(var y = 0; y < midpointWidth; y += pow2) {
			//console.log("x " + x + "   y " + y);
			calculateMiddle(x, y, pow2, pow2);
			
			calculateVerticalSide(x, y + pow2/2, pow2/2);
			calculateVerticalSide(x + pow2, y + pow2/2, pow2/2);
			calculateHorizontalSide(x + pow2/2, y, pow2/2);
			calculateHorizontalSide(x + pow2/2, y + pow2, pow2/2);
			
		}
	}

}



function randomizeCorners() {
	setPixel(0, 0, Math.random() * 255);
	setPixel(midpointWidth, 0, Math.random() * 255);
	setPixel(0, midpointWidth, Math.random() * 255);
	setPixel(midpointWidth, midpointWidth, Math.random() * 255);
}

function calculateHorizontalSide(x, y, dist) {
	var c1 = getPixel(x - dist, y);
	var c2 = getPixel(x + dist, y);
	var avg = (c1 + c2)/2;
	
	setPixel(x, y, avg);
}

function calculateVerticalSide(x, y, dist) {
	//console.log("Calculating vertical for " + x + " " + y);
	var c1 = getPixel(x, y - dist);
	var c2 = getPixel(x, y + dist);
	var avg = (c1 + c2)/2;
	
	setPixel(x, y, avg);
}


function calculateMiddle(x, y, width, height) {
	var c1 = getPixel(x, y);
	var c2 = getPixel(x + width, y);
	var c3 = getPixel(x, y + height);
	var c4 = getPixel(x + width, y + height);
	
	var avg = (c1 + c2 + c3 + c4) / 4;
	
	var rand = Math.round( (Math.random() * width / this.max - width / this.max * 0.5) * 255 * 0.2);
	
	//console.log("Rand is " + rand);
	
	avg += rand;
	
	//console.log("Setting pixel " + (x + width / 2) + " " + (y  + height / 2));
	setPixel(x + width / 2, y + height / 2, avg);
}

function getPixel(x, y) {
	return midpointData[(y * midpointWidth + x)];
}

function setPixel(x, y, v) {
	//console.log("Setting value " + x + " " + y + " to " +  v);
	midpointData[(y * midpointWidth + x)] = v;
}