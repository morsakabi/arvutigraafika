var Chunk = function (pos, data, faces, vertices) {
    this.pos = pos;
    this.data = data;
    this.faces = faces;
    this.vertices = vertices;
    
    return this;
};


Chunk.prototype = {
    buildMesh: function() {
        var material = new THREE.MeshLambertMaterial({vertexColors: THREE.FaceColors});
        
        var geometry = new THREE.Geometry();
        //Got to rebuild the vertices as vector3s instead of plain objects
        var t0 = performance.now();
        for(var i = 0; i < this.vertices.length; i++) {
            var vec = new THREE.Vector3(this.vertices[i][0], this.vertices[i][1], this.vertices[i][2]);
            geometry.vertices.push(vec);
        }
        var t1 = performance.now();
        //console.log('Chunk vert loop took', (t1 - t0).toFixed(4), 'milliseconds');
        
        //Got to rebuild the faces as Face3 instead of plain objects
        t0 = performance.now();
        for(var i = 0; i < this.faces.length; i++) {
            var face = new THREE.Face3(this.faces[i][0], this.faces[i][1], this.faces[i][2]);
            face.color.setHex(this.faces[i].color);
            geometry.faces.push(face);
        }
        t1 = performance.now();
        //console.log('Chunk face loop took', (t1 - t0).toFixed(4), 'milliseconds');

        geometry.computeFaceNormals();
        
        var mesh = new THREE.Mesh(geometry, material);
        this.mesh = mesh;
    }
};